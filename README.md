Logistic Regression ile Telefon Fiyat Tahmini Uygulaması

Data seti indirmek için https://www.kaggle.com/iabhishekofficial/mobile-price-classification/download

Requirements

Python 3.7.10

NumPy 1.19.5

Pandas 1.1.5

OS Linux-4.19.112+-x86_64-with-Ubuntu-18.04-bionic

MatPlotLib 3.2.2

Seaborn 0.11.1

Sklearn 0.22.2.post1

Soru ve/veya sorun halinde kcemozdemir ile iletişime geçiniz